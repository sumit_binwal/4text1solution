#import <Foundation/Foundation.h>
#import<sqlite3.h>

@interface DatabaseHandler : NSObject
{
	sqlite3 *database;
    const char *error;
	
}

// Declaration of DataBase Creation and Database Path Location fetching Methods
-(NSString *) dataFilePath;
+(void) checkAndCreateDatabase;
-(void)executeQuery:(NSString *)query;
-(NSMutableArray *)getPuzzleData:(NSString *)query;
-(BOOL)checkStatus:(NSString*)day;

@end
