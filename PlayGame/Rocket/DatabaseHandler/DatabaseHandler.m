//
//  DatabaseHandler.m
//
//
//  Created by Amzad on 10/10/12.
//
//

#import "DatabaseHandler.h"

@implementation DatabaseHandler

#pragma mark -
#pragma mark Creating Database if that not exists

/*==================================================================
 GET DATABASE PATH IN DOCUMENT DIRECTORY
 ==================================================================*/
-(NSString *) dataFilePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    
	return [documentsDirectory stringByAppendingPathComponent:DatabaseName];
}

+(void) checkAndCreateDatabase{
	// check if the SQL database has already been saved to the users phone, if not then copy it over
	BOOL success;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:[documentsDirectory stringByAppendingPathComponent:DatabaseName]];
	
	// If the database already exists then return without doing anything
	if(success)
		return;
	else
		NSLog(@"Not Existed");
	
	// If not then proceed to copy the database from the application to the users filesystem
	
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DatabaseName];
	
	// Copy the database from the package to the users filesystem
    NSLog(@"databasePathFromApp==> %@",databasePathFromApp);
    
    NSLog(@"PATH==> %@",[documentsDirectory stringByAppendingPathComponent:DatabaseName]);
    
	[fileManager copyItemAtPath:databasePathFromApp toPath:[documentsDirectory stringByAppendingPathComponent:DatabaseName] error:nil];
	
}
/*==================================================================
 METHOD FOR INSERTING DATA IN DATABASE
 ==================================================================*/
-(void)executeQuery:(NSString *)query
{
//	NSLog(@"QUERY:=%@",query);
    char *err;
    
    sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &err);
    
    if(sqlite3_open([[self dataFilePath] UTF8String], &database) == SQLITE_OK)
	{
        if (sqlite3_exec(database, [query cStringUsingEncoding:NSUTF8StringEncoding], NULL, NULL, &err)  == SQLITE_OK)
        {
//            NSLog(@"record Statement Compiled!!");
        }
        else
        {
            NSLog(@"query Statement Not Compiled");
            NSLog(@"%s",error);
        }
		sqlite3_exec(database, "COMMIT TRANSACTION", NULL, NULL, &err);
        
		sqlite3_close(database);
    }
}
//-(void)executeQuery:(NSString *)query
//{
//	NSLog(@"QUERY:=%@",query);
//    char *err;
//    
//    if(sqlite3_open([[self dataFilePath] UTF8String], &database) == SQLITE_OK)
//	{
//        if (sqlite3_exec(database, [query UTF8String], NULL, NULL, &err)  == SQLITE_OK)
//        {
//            NSLog(@"record Statement Compiled!!");
//        }
//        else
//        {
//            NSLog(@"query Statement Not Compiled");
//            NSLog(@"%s",error);
//        }
//    }
//}

/*==================================================================
 METHOD FOR Fetching DATA FROM DATABASE
 ==================================================================*/

-(NSMutableArray *)getPuzzleData:(NSString *)query
{
	NSString *idToReturn=@"";
	NSMutableArray *returnArray = [NSMutableArray new];
    
	if(sqlite3_open([[self dataFilePath] UTF8String], &database) == SQLITE_OK)
	{
		sqlite3_stmt *statement;
		if (sqlite3_prepare_v2(database, [query cStringUsingEncoding:NSUTF8StringEncoding],-1, &statement, &error)==SQLITE_OK)
		{
			while(sqlite3_step(statement)==SQLITE_ROW)
			{
				NSMutableDictionary *temp= [NSMutableDictionary new];
				const char *s;
                
                s=(char *)sqlite3_column_text(statement, 0);
				if(s==NULL)
				{
					idToReturn=@"";
				}
				else
				{
					idToReturn =[NSString stringWithUTF8String:s];
                    
				}
				[temp setObject:idToReturn forKey:@"id"];
                
                
				s=(char *)sqlite3_column_text(statement, 1);
				if(s==NULL)
				{
					idToReturn=@"";
				}
				else
				{
					idToReturn =[NSString stringWithUTF8String:s];
				}
				[temp setObject:idToReturn forKey:@"PuzzleName"];

                
                s=(char *)sqlite3_column_text(statement, 2);
				if(s==NULL)
				{
					idToReturn=@"";
				}
				else
				{
					idToReturn =[NSString stringWithUTF8String:s];
				}
				[temp setObject:idToReturn forKey:@"Option1"];

                s=(char *)sqlite3_column_text(statement, 3);
				if(s==NULL)
				{
					idToReturn=@"";
				}
				else
				{
					idToReturn =[NSString stringWithUTF8String:s];
				}
				[temp setObject:idToReturn forKey:@"Option2"];

                s=(char *)sqlite3_column_text(statement, 4);
				if(s==NULL)
				{
					idToReturn=@"";
				}
				else
				{
					idToReturn =[NSString stringWithUTF8String:s];
				}
				[temp setObject:idToReturn forKey:@"Option3"];
                
                s=(char *)sqlite3_column_text(statement, 5);
				if(s==NULL)
				{
					idToReturn=@"";
				}
				else
				{
					idToReturn =[NSString stringWithUTF8String:s];
				}
				[temp setObject:idToReturn forKey:@"Option4"];
                
                s=(char *)sqlite3_column_text(statement, 6);
				if(s==NULL)
				{
					idToReturn=@"";
				}
				else
				{
					idToReturn =[NSString stringWithUTF8String:s];
				}
				[temp setObject:idToReturn forKey:@"SolveFlag"];
                
				if (temp != nil)
				{
					[returnArray addObject:temp];
                    //NSLog(@"temp == %@",temp);
					temp = nil;
				}
				
			}
			sqlite3_finalize(statement);
			sqlite3_close(database);
		}
        else
        {
            NSLog(@"QUERY Statement Not Compiled: %@  \n Error :%s",query,error);
        }
	}
	return returnArray;
}


-(BOOL)checkStatus:(NSString*)puzzleId
{
    BOOL statusVal = NO;
    NSString *query=[NSString stringWithFormat:@"SELECT SolveFlag FROM PuzzleTable WHERE id=%@",puzzleId];
    
	if(sqlite3_open([[self dataFilePath] UTF8String], &database) == SQLITE_OK)
	{
		sqlite3_stmt *statement;
		if (sqlite3_prepare_v2(database, [query cStringUsingEncoding:NSUTF8StringEncoding],-1, &statement, &error)==SQLITE_OK)
		{
			while(sqlite3_step(statement)==SQLITE_ROW)
			{
				const char *s;
                s=(char *)sqlite3_column_text(statement, 0);
                
                statusVal=(s==NULL)?NO:YES;
        
			}
			sqlite3_finalize(statement);
			sqlite3_close(database);
		}
        else
        {
            NSLog(@"QUERY Statement Not Compiled: %@  \n Error :%s",query,error);
        }
	}
    
    return statusVal;
}

@end
