//
//  Config.h
//  WhatzzApp
//
//  Created by Konstant on 22/05/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>



//#define font_regular16  ((UIFont *)[UIFont fontWithName:(NSString *)(@"HelveticaNeue") size:(CGFloat)(16)])

//#define FIX_IOS_7_LAY_OUT_ISSUE  if([self respondsToSelector:@selector(edgesForExtendedLayout)])self.edgesForExtendedLayout=UIRectEdgeNone;

@interface Config : NSObject
{

}

//configuration section...


/**** set intial coins ****/
extern int         coins;
extern int         winCoins;
extern int         deleteCoins;
extern int         showCoins;
extern int         skipCoins;
extern int         bonusCoins;
extern int         rateAppCoins;
extern int         shareOnFbCoins;
extern int         watchVideoAdsCoins;

/**** variables for current device  ****/
extern BOOL isIpad;
extern BOOL isiPhone5;

extern  NSMutableArray *puzzleArray;
extern  NSMutableDictionary *puzzleDictionary;

extern  NSString	*SiteAPIURL;
extern  NSString    *rateURL;

/**** Database name ****/
extern  NSString    *DatabaseName;
extern  NSString    *DatabasePath;

/**** All key ****/
extern  NSString        *flurryKey;
extern  NSString        *AppraterId;
extern  NSString        *AdColonyAppID;
extern  NSString        *AdColonyZoneID;
extern  NSString        *ChartboostAppID;
extern  NSString        *chartboostSignatureKey;
extern  NSString		*revMobKey;
extern  NSString        *iTunesAppID;

extern  NSString		*currentPuzzleNumber;

extern  NSString		*moreAppURL;
extern  NSString		*moreAppButtonUrl;

extern  NSString		*subjectMsg;
extern  NSString		*mailAddress;

extern  NSString		*currentDateTime;

/******** variables for messages ************/
extern  NSString		*FBHomeMsg;
extern  NSString		*FBSpecialDayMsg;
extern  NSString		*FBhelpMsg;

extern  NSString		*twitterMsg;
extern  NSString		*twitterSpecialDayMsg;

extern  NSString		*rateThisAppMsg;

extern  NSString        *Congrats;
extern  NSString        *rightString;

/******** variables for LocalNotification ************/
extern  NSString		*localNotificationMsg;
extern UILocalNotification *localNotification;

/******** variables for IAP ************/
extern  NSArray         *productArr;
extern  NSString		*removeAddProductId;

@end
