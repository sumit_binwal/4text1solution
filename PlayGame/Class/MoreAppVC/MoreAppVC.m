//
//  MoreAppVC.m
//  PlayGame
//
//  Created by Chandan Kumar on 10/04/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import "MoreAppVC.h"

@interface MoreAppVC ()

@end

@implementation MoreAppVC

@synthesize moreArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"background"]]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark IBActions
-(IBAction)backButtonPressed:(id)sender
{
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark- UITableView Delegate and Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (moreArray.count )
    {
        return isIpad?120.0f:65.0f;
    }
    
    return 0.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return moreArray.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier: nil];


    UIImageView *cellBackgroundImage;
    UIImageView *moreAppImage;
    UILabel  *appNameLabel;
    UIButton *rightButton;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellBackgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(isIpad?120:14, isIpad?9:5, isIpad?531:294, isIpad?102:55)];
        [cellBackgroundImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"more_app_list_bg"]]];
        [cell.contentView addSubview:cellBackgroundImage];
        
        
        moreAppImage = [[UIImageView alloc] initWithFrame:CGRectMake(isIpad?127:18, isIpad?16:8, isIpad?86:48, isIpad?86:48)];
        [cell.contentView addSubview:moreAppImage];
        
        
        //Add label for appName
        appNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(isIpad?228:78, isIpad?16:8, isIpad?270:148, isIpad?86:48)];
        appNameLabel.numberOfLines=0;
        [appNameLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:isIpad?22.0f:12.0f]];
        appNameLabel.textAlignment = NSTextAlignmentLeft;
        [appNameLabel setBackgroundColor:[UIColor clearColor]];
        [appNameLabel setTextColor: [UIColor whiteColor]];
        [cell.contentView addSubview:appNameLabel];

        //Add right button
        rightButton =[UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.tag=indexPath.row;
        [rightButton setFrame:CGRectMake(isIpad?522:240, isIpad?34:18, isIpad?105:58, isIpad?50:28)];
        [rightButton setBackgroundImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"free_but"]] forState:UIControlStateNormal];
        [rightButton.titleLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:isIpad?22.0f:12.0f]];
        [rightButton addTarget:self action:@selector(openCoolAppButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:rightButton];
    }
    
    [cellBackgroundImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"more_app_list_bg"]]];
    [moreAppImage setImageWithURL:[NSURL URLWithString:[[moreArray objectAtIndex:indexPath.row] valueForKey:@"Img"]] placeholderImage:[UIImage imageNamed:@"no_image"]];
    [appNameLabel setText:[[moreArray objectAtIndex:indexPath.row] valueForKey:@"Name"]];
    
    if ([[[moreArray objectAtIndex:indexPath.row] valueForKey:@"isPaid"] isEqualToString:@"1"])
        [rightButton setTitle:@"Paid" forState:UIControlStateNormal];
    else
        [rightButton setTitle:@"Free" forState:UIControlStateNormal];
    
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (IBAction)openCoolAppButtonPressed:(UIButton*)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[moreArray objectAtIndex:sender.tag] valueForKey:@"Url"]]];
}
@end
