//
//  InAppPurrchaseVC.m
//  PlayGame
//
//  Created by Chandan Kumar on 09/04/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import "InAppPurrchaseVC.h"
#import "GetPremiumVC.h"
#import "RageIAPHelper.h"

@interface InAppPurrchaseVC ()

@end

@implementation InAppPurrchaseVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showInAppPurchase];
}
- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [(UIImageView*)[self.view viewWithTag:100] setImage:[UIImage imageNamed:isIpad?@"iap_pop_upbg_iPad":isiPhone5?@"iap_pop_upbg_5":@"iap_pop_upbg"]];
    
    extraProductIds = @[NSLocalizedString(@"rateApp", nil),
                      NSLocalizedString(@"shareOnFb", nil),
                      NSLocalizedString(@"watchVideo", nil),
                      ];
 
    _topConstraint.constant = isiPhone5?67:40;
    _crossConstraint.constant = isiPhone5?42:20;
    

}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark InAppPurchase
- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [productArr enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            [_iapTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            *stop = YES;
        }
    }];
}

- (void)reload
{
    [_iapTableView setDelegate:self];
    [_iapTableView setDataSource:self];
    
    if (productArr.count == 0)
    {
        [_iapActivityIndicator startAnimating];
        [_iapActivityIndicator setHidden:NO];
        
        [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
                productArr = products;
                
                [self setUpArrayAndReload];
            }
        }];
    }
    else
    {
        [self setUpArrayAndReload];
    }
}

-(void) setUpArrayAndReload
{
    if (sortProducts.count)
        [sortProducts removeAllObjects];
    else
        sortProducts = [[NSMutableArray alloc] init];
    
    if (productArr.count) {
        for (SKProduct *product in productArr)
        {
            if (![product.productIdentifier isEqualToString:removeAddProductId])
                [sortProducts addObject:product];

        }
    }
        
    
    [_iapTableView reloadData];

    [_iapActivityIndicator stopAnimating];
    [_iapActivityIndicator setHidden:YES];
}
#pragma mark-
#pragma mark IBActions
-(IBAction)CrossButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark-
#pragma mark Other methods
-(void)showInAppPurchase
{
    if ([SKPaymentQueue canMakePayments])
    {
        [self reload];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"In-App Purchases unavailable" message:@"Please enable In-App purchases in the settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)shareOnFbToIncreaseCoin
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [mySLComposerSheet addImage:[UIImage imageNamed:@"app_icon_144"]];
        [mySLComposerSheet setInitialText:FBHomeMsg];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                {
                    coins +=shareOnFbCoins;
                    [DEFAULTS setObject:[NSNumber numberWithInt:coins] forKey:@"numCoins"];
                    [DEFAULTS synchronize];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedCoinsAndAds" object:nil];
                    
                    NSLog(@"Post Sucessful");
                    break;
                }
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Facebook Account" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in Setting." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}

-(void)rateAlert
{
    [Appirater setAppId:AppraterId];
    [Appirater setDaysUntilPrompt:0];
    [Appirater setDebug:YES];
    
    NSLog(@"FLAG %hhd",((NSNumber*)[DEFAULTS objectForKey:@"rate"]).boolValue);
    
//    [DEFAULTS setValue:@(NO) forKey:@"rate"];
//    [DEFAULTS synchronize];
    
    if(((NSNumber*)[DEFAULTS objectForKey:@"rate"]).boolValue == NO)
    {
        [Appirater appLaunched:YES];
    }
    else
    {
    }
}

-(void)watchVideo
{
//    [AdColony playVideoAdForZone:AdColonyZoneID withDelegate:nil withV4VCPrePopup:YES andV4VCPostPopup:YES];
    
    [AdColony playVideoAdForZone:AdColonyZoneID withDelegate:nil];
}

#pragma mark -
#pragma mark UITableView Delegate and Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int productCount = sortProducts.count;
    if (productCount || extraProductIds.count)
    {
        return isIpad?101.0f:55.0f;
    }
    
    return 0.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int productCount = sortProducts.count;
    //   return 6;
    return productCount + extraProductIds.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"inAppCellIdentifier";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    
    UIImageView *cellBackgroundImage;
    UIImageView *coinImage;
    UILabel  *coinLabel;
    UILabel  *dollarLabel;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cellBackgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(isIpad?46:23, 2, isIpad?416:231, isIpad?93:51)];
        cellBackgroundImage.tag=1;
        [cellBackgroundImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"getmore_coin_green_but"]]];
        [cell.contentView addSubview:cellBackgroundImage];
        
        
        coinImage = [[UIImageView alloc] initWithFrame:CGRectMake(isIpad?75:39, cell.frame.origin.y+(isIpad?+25:6), isIpad?36:20, cell.frame.size.height+(isIpad?+4:0))];
        coinImage.tag=2;
        [coinImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"6_coins"]]];
        coinImage.contentMode=UIViewContentModeCenter;
        [cell.contentView addSubview:coinImage];
        
        
        //Add label for coinLabel
        coinLabel=[[UILabel alloc]initWithFrame:CGRectMake(isIpad?102:54, cell.frame.origin.y+isIpad?25:4, isIpad?88:48, cell.frame.size.height)];
        coinLabel.tag=3;
        [coinLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:isIpad?26.0f:15.0f]];
        coinLabel.textAlignment = NSTextAlignmentCenter;
        [coinLabel setBackgroundColor:[UIColor clearColor]];
        [coinLabel setTextColor: [UIColor whiteColor]];
        [cell.contentView addSubview:coinLabel];
        
        
        //Add label for dollar
        dollarLabel=[[UILabel alloc]initWithFrame:CGRectMake(isIpad?200:105, cell.frame.origin.y+isIpad?25:4, isIpad?245:138, cell.frame.size.height)];
        dollarLabel.tag=4;
        [dollarLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:isIpad?23.0f:15.0f]];
        dollarLabel.textAlignment = NSTextAlignmentRight;
        [dollarLabel setBackgroundColor:[UIColor clearColor]];
        [dollarLabel setTextColor: [UIColor whiteColor]];
        [cell.contentView addSubview:dollarLabel];
    }
    else
    {
        cellBackgroundImage = (UIImageView*) [cell viewWithTag:1];
        coinImage = (UIImageView*) [cell viewWithTag:2];
        coinLabel = (UILabel*) [cell viewWithTag:3];
        dollarLabel = (UILabel*) [cell viewWithTag:4];
    }
    
     int productCount = sortProducts.count;
    
    if (productCount==indexPath.row || productCount+1==indexPath.row || productCount+2==indexPath.row) {
        [cellBackgroundImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"getmore_coin_bule_but"]]];
        
        if (productCount==indexPath.row) {
            [dollarLabel setText:[extraProductIds objectAtIndex:0]];
            [coinImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:[NSString stringWithFormat:@"%d_coins",rateAppCoins]]]];
            [coinLabel setText:[NSString stringWithFormat:@"+%d",rateAppCoins]];
        }
        else if (productCount+1==indexPath.row) {
            [dollarLabel setText:[extraProductIds objectAtIndex:1]];
            [coinImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:[NSString stringWithFormat:@"%d_coins",shareOnFbCoins]]]];
            [coinLabel setText:[NSString stringWithFormat:@"+%d",shareOnFbCoins]];
        }
        else if (productCount+2==indexPath.row) {
            [dollarLabel setText:[extraProductIds objectAtIndex:2]];
            [coinImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:[NSString stringWithFormat:@"%d_coins",watchVideoAdsCoins]]]];
            [coinLabel setText:[NSString stringWithFormat:@"+%d",watchVideoAdsCoins]];
        }
    }
    else
    {
        [cellBackgroundImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"getmore_coin_green_but"]]];
        
        SKProduct *product = sortProducts[indexPath.row];
        [[AppDelegate getSharedInstance]._priceFormatter setLocale:product.priceLocale];
 
        if ([product.productIdentifier isEqualToString:removeAddProductId]) {
            cell.userInteractionEnabled = ((NSNumber*)[DEFAULTS objectForKey:removeAddProductId]).boolValue;
        }
        
        if ([product.localizedTitle intValue]>=3 && [product.localizedTitle intValue]<5 )
            [coinImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"3_coins"]]];
        else if ([product.localizedTitle intValue]>=5 && [product.localizedTitle intValue]<100 )
            [coinImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"5_coins"]]];
        else if ([product.localizedTitle intValue]>=10 && [product.localizedTitle intValue]<100 )
            [coinImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"5_coins"]]];
        else if ([product.localizedTitle intValue]>=100)
            [coinImage setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"6_coins"]]];
        
//        [coinLabel setText:[NSString stringWithFormat:@"+%d",[product.localizedTitle intValue]]];
        [coinLabel setText:[NSString stringWithFormat:@"+%d",100]];
        [dollarLabel setText:[[AppDelegate getSharedInstance]._priceFormatter stringFromNumber:product.price]];
        
        NSLog(@"product.localizedTitle=>%@  product.price=>%@  product.localizedDescription=>%@",product.localizedTitle,product.price,product.localizedDescription);
        
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int productCount = sortProducts.count;
    
    if (productCount==indexPath.row || productCount+1==indexPath.row || productCount+2==indexPath.row) {
        
        if (productCount==indexPath.row) {
            [self rateAlert];
        }
        else if (productCount+1==indexPath.row) {
            [self shareOnFbToIncreaseCoin];
        }
        else if (productCount+2==indexPath.row) {
            NSLog(@"Watch video add");
            [self watchVideo];
        }
    }
    else
    {
        SKProduct *product = sortProducts[indexPath.row];
        NSLog(@"Buying %@...", product.productIdentifier);
        
        if ([product.productIdentifier isEqualToString:removeAddProductId])
        {
            GetPremiumVC *gvc = [[GetPremiumVC alloc] initWithNibName:[CommonFunctions getNibNameForName:@"GetPremiumVC"] bundle:nil];
            gvc.view.backgroundColor = [UIColor clearColor];
            self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
            
            CATransition *slide = [CATransition animation];
            slide.type = kCATransitionPush;
            slide.subtype = kCATransitionFromTop;
            slide.duration = 0.4;
            slide.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
            slide.removedOnCompletion = YES;
            [gvc.view.layer addAnimation:slide forKey:@"slidein"];
            
            [self presentViewController:gvc animated:FALSE completion:nil];
        } else
             [[RageIAPHelper sharedInstance] buyProduct:product];
        
    }
}


@end
