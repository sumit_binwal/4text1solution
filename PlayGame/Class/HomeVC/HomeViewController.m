    //
//  HomeViewController.m
//  PlayGame
//
//  Created by Chandan Kumar on 04/03/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import <AdColony/AdColony.h>
#import "HomeViewController.h"
#import "SettingsViewController.h"
#import "PlayVC.h"
#import "Chartboost.h"
#import "GetPremiumVC.h"
#import "MoreAppVC.h"

@interface HomeViewController ()<UIAlertViewDelegate, ChartboostDelegate>

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"AppName", nil);
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    [self setUpMainView];
    [self updateAdSwitch];
    
    [self performSelectorOnMainThread:@selector(startTimer) withObject:nil waitUntilDone:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteObserver" object:nil];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"background"]]];
  
    [self.navigationController setNavigationBarHidden:YES];
    
     NSLog(@"Roboto ==>%@",[UIFont fontNamesForFamilyName:@"Roboto Slab" ]);
     NSLog(@"Romy ==>%@",[UIFont fontNamesForFamilyName:@"Romy"]);
    
    [_puzzleNumLevel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?36:20]];
    
    _topConstraint.constant = isiPhone5?57:35;
    _firstMidConstraint.constant = isiPhone5?22:5;
    _midConstraint.constant = isiPhone5?22:5;
    _bottomConstraint.constant = isiPhone5?57:38;
    
    [_switchButton setThumbTintColor:[UIColor whiteColor]];
    [_switchButton setOnTintColor:UIColorFromRedGreenBlue(3, 34, 117)];
    
    //*******Create Database class object ********//
    dbRecord =[[DatabaseHandler alloc] init];

    [self performSelectorInBackground:@selector(parsingMoreApplications) withObject:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAdSwitch) name:@"updatedAds" object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [refreshTimer invalidate];
    refreshTimer = nil;
}
- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [refreshTimer invalidate];
    refreshTimer = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark -
# pragma mark IBAction
-(IBAction)showSettings:(id)sender
{
    SettingsViewController *vc = [[SettingsViewController alloc] initWithNibName:[CommonFunctions getNibNameForName:@"SettingsViewController"] bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)playButtonPressedTouchUpInside:(id)sender
{
    PlayVC *pvc = [[PlayVC alloc] initWithNibName:[CommonFunctions getNibNameForName:@"PlayVC"] bundle:nil];
    [self.navigationController pushViewController:pvc animated:YES];
}
- (IBAction)faceBookButtonPressed:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [mySLComposerSheet addImage:[UIImage imageNamed:@"app_icon_144"]];
        [mySLComposerSheet setInitialText:FBHomeMsg];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Facebook Account" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in Setting." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}

- (IBAction)twitterButtonPressed:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *composeController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [composeController setInitialText:twitterMsg];
        
        [self presentViewController:composeController animated:YES completion:nil];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {

                NSLog(@"delete");
            }
            else
            {
                NSLog(@"post");
            }
            
            [composeController dismissViewControllerAnimated:YES completion:Nil];
        };
        composeController.completionHandler =myBlock;
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Twitter Account" message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Setting." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        [alert show];
        
    }
}

- (IBAction)mailButtonPressed:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:subjectMsg];
        NSArray *toRecipients = [NSArray arrayWithObjects:mailAddress, nil];
        [mailer setToRecipients:toRecipients];
        
        [self presentViewController:mailer animated:TRUE completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Oops!"
                                    message:@"Please configure atleast one account for sending mail." delegate:nil
                          cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (IBAction)switchHideButtonPressed:(UISwitch*)sender
{
    if (!sender.on) {
        GetPremiumVC *gvc = [[GetPremiumVC alloc] initWithNibName:[CommonFunctions getNibNameForName:@"GetPremiumVC"] bundle:nil];
        gvc.view.backgroundColor = [UIColor clearColor];
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        CATransition *slide = [CATransition animation];
        slide.type = kCATransitionPush;
        slide.subtype = kCATransitionFromTop;
        slide.duration = 0.4;
        slide.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        slide.removedOnCompletion = YES;
        [gvc.view.layer addAnimation:slide forKey:@"slidein"];
        
        [self.navigationController presentViewController:gvc animated:FALSE completion:nil];
    }
    else
    {
        [DEFAULTS setObject:@(YES) forKey:removeAddProductId];
        [DEFAULTS synchronize];
    }
}

- (IBAction)moreGames:(id)sender
{
    Chartboost *cb = [Chartboost sharedChartboost];
    [cb showMoreApps];
}
- (IBAction)hideAds:(id)sender {
    Chartboost *cb = [Chartboost sharedChartboost];
    [cb showMoreApps];
}

# pragma mark -
# pragma mark  Other methods
-(void)updateAdSwitch
{
    NSLog(@"FLAG %hhd",((NSNumber*)[DEFAULTS objectForKey:removeAddProductId]).boolValue);
    _switchButton.on=((NSNumber*)[DEFAULTS objectForKey:removeAddProductId]).boolValue;
}

-(void)setUpMainView
{
    //******************Insert Records First Time into Database **********************//
    if (![DEFAULTS boolForKey:@"isFirst"])
    {
        [DEFAULTS setBool:YES forKey:@"isFirst"];
        [DEFAULTS setObject:currentPuzzleNumber forKey:@"currentPuzzleNo"];
        [DEFAULTS setObject:[NSNumber numberWithInt:coins] forKey:@"numCoins"];
        [DEFAULTS synchronize];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"PuzzleList" ofType:@"plist"];
        puzzleDictionary = [[NSMutableDictionary alloc]initWithContentsOfFile:path];
        puzzleArray = [[NSMutableArray alloc] init];
        
        NSMutableArray *tempPuzzleArray = [[NSMutableArray alloc] init];
        for (NSString *key in [puzzleDictionary allKeys]) {
            // Pick which key is the one you want
            [tempPuzzleArray addObject:key];
        }
        
        for (id key in tempPuzzleArray) {
            // Pick which key is the one you want
            
            NSMutableArray *arr = [NSMutableArray  arrayWithArray:[puzzleDictionary valueForKey:key]];
            
            query = [NSString stringWithFormat:@"INSERT INTO PuzzleTable (\"PuzzleName\",\"Option1\",\"Option2\",\"Option3\",\"Option4\") Values('%@','%@','%@','%@','%@')",key,[arr objectAtIndex:0],[arr objectAtIndex:1],[arr objectAtIndex:2],[arr objectAtIndex:3]];
            
            NSLog(@"%@",query);
            [dbRecord performSelector:@selector(executeQuery:) withObject:query];
        }
    }
    
    if (puzzleArray.count)
    {
        [puzzleArray removeAllObjects];
        puzzleArray = [NSMutableArray arrayWithArray:[dbRecord getPuzzleData:@"SELECT * FROM PuzzleTable"]];
    }
    else
        puzzleArray = [[NSMutableArray alloc]initWithArray:[dbRecord getPuzzleData:@"SELECT * FROM PuzzleTable"]];
    
    //    NSLog(@"puzzleArray====> %@",puzzleArray);
    
    _puzzleNumLevel.text = [NSString stringWithFormat:@"%d",puzzleArray.count];
}

/***** this is for start auto-refresh functionality******/
-(void) startTimer
{
    int refreshValue=1*60*60;
    refreshTimer=[NSTimer scheduledTimerWithTimeInterval:refreshValue target:self selector:@selector(onTickTimer:) userInfo: nil repeats:YES];
}

-(void)onTickTimer:(NSTimer *)timer
{
    /******** get all new orders details *******/
    
    [NSThread detachNewThreadSelector:@selector(setUpMoreScrollview) toTarget:self withObject:nil];
}

-(void)parsingMoreApplications
{
    moreArray = [[NSMutableArray alloc]init];
    
    NSData *moreAppData = [NSData dataWithContentsOfURL:[NSURL URLWithString:moreAppURL]];
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:moreAppData];
    [parser setDelegate:self];
    [parser parse];
    
    
    [self performSelectorOnMainThread:@selector(setUpMoreScrollview) withObject:nil waitUntilDone:NO];
}

-(void)setUpMoreScrollview
{
//    NSLog(@"MoreArray=>%@",moreArray);
    if (moreArray.count) {
        [_moreScrollview setHidden:NO];
        [_moreScrollview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"main_screen_footer"]]]];
        
        //Add product image
        for (UIView *v in _moreScrollview.subviews) {
            [v removeFromSuperview];
        }
        
        //Add label for MoreCoolApp
        UILabel *moreAppLabel=[[UILabel alloc]initWithFrame:CGRectMake(isIpad?12:8, isiPhone5?8:0, isIpad?160:80, isIpad?36:18)];
        [moreAppLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:isIpad?18.0f:10.0f]];
        [moreAppLabel setTextColor: UIColorFromRedGreenBlue(184, 213, 254)];
        [moreAppLabel setBackgroundColor:[UIColor clearColor]];
        [moreAppLabel setText:NSLocalizedString(@"MoreCoolApp", nil)];
        [_moreScrollview addSubview:moreAppLabel];
        
        
        NSMutableArray *randomMoreArray = [[NSMutableArray alloc]init];
        NSMutableArray *moreCopyArray =  [[NSMutableArray alloc]initWithArray:moreArray];
        for (int i=0; i<moreArray.count; i++)
        {
            int indx = arc4random() % [moreCopyArray count];
            NSMutableDictionary *appDic = [moreCopyArray objectAtIndex:indx];
            [randomMoreArray addObject:appDic];
            [moreCopyArray removeObjectAtIndex:indx];
        }
        
        CGRect frame = _moreScrollview.bounds;
        frame.size.width = frame.size.width - (isIpad?100:58);
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:frame];
        scrollView.backgroundColor = [UIColor clearColor];
        [scrollView setShowsHorizontalScrollIndicator:NO];
        
        for (int i=0; i<moreArray.count; i++)
        {
            float width = (isIpad?205:114) + (isIpad?16:8);
            float height = isIpad?82:45;
            
            // Add product outer Image
            CGRect _frame=CGRectMake((isIpad?12:8)+i*(width+(isIpad?16:7)), isIpad?46:isiPhone5?27:17, width ,height);
            
            //Add blue Back Image
            UIImageView *moreBlueSquare = [[UIImageView alloc]initWithFrame:_frame];
            [moreBlueSquare setImage:[UIImage imageNamed:@"more_app_list_bg"]];
//            moreBlueSquare.layer.cornerRadius=20.0f;
            [scrollView addSubview:moreBlueSquare];
            
            //Add application image
            width = isIpad?68:40;
            height = isIpad?68:40;
            UIImageView*moreAppImageView =[[UIImageView alloc] initWithFrame:CGRectMake(_frame.origin.x+(isIpad?4:2), _frame.origin.y+(isIpad?6:2),width,height)];
            [moreAppImageView setImageWithURL:[NSURL URLWithString:[[randomMoreArray objectAtIndex:i] valueForKey:@"Img"]] placeholderImage:[UIImage imageNamed:@"no_image"]];
            [scrollView addSubview:moreAppImageView];
            
            //Add label for product name
            UILabel *appNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(_frame.origin.x+width+(isIpad?18:10), _frame.origin.y+(isIpad?4:0), width+(isIpad?60:20), height)];
            appNameLabel.numberOfLines=0;
            [appNameLabel setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:isIpad?18.0f:10.0f]];
            [appNameLabel setTextColor: UIColorFromRedGreenBlue(184, 213, 254)];
            [appNameLabel setTextAlignment:NSTextAlignmentLeft];
            [appNameLabel setBackgroundColor:[UIColor clearColor]];
            [appNameLabel setText:[[randomMoreArray objectAtIndex:i] valueForKey:@"Name"]];
            [scrollView addSubview:appNameLabel];
            
            //Add top clear button
            UIButton *topButton =[UIButton buttonWithType:UIButtonTypeCustom];
            [topButton setFrame:_frame];
            [topButton setTitle:[[randomMoreArray objectAtIndex:i] valueForKey:@"Url"] forState:UIControlStateNormal];
            [topButton.titleLabel setFont:[UIFont systemFontOfSize:0]];
            [topButton addTarget:self action:@selector(openCoolAppButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:topButton];
            
             [_moreScrollview addSubview:scrollView];
        }
        
        //Add more Button
        UIButton *moreButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [moreButton setFrame:CGRectMake(2*((isIpad?249:114) + (isIpad?88:19)),(isIpad?46:isiPhone5?27:17), isIpad?84:47, isIpad?81:45)];
        [moreButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [moreButton setImage:[UIImage imageNamed:[CommonFunctions getImageNameForName:@"more_but"]] forState:UIControlStateNormal];
        [moreButton addTarget:self action:@selector(moreCoolAppTouchUpInside) forControlEvents:UIControlEventTouchUpInside];
        [_moreScrollview addSubview:moreButton];
        
        scrollView.contentSize = CGSizeMake((isIpad?240:130)*moreArray.count, _moreScrollview.frame.size.height);
    }
    else
        [_moreScrollview setHidden:YES];
}

- (void)openCoolAppButtonPressed:(UIButton*)sender
{
    NSLog(@"%@",sender.titleLabel.text);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sender.titleLabel.text]];
}
- (void)moreCoolAppTouchUpInside {
    MoreAppVC *mvc = [[MoreAppVC alloc] initWithNibName:[CommonFunctions getNibNameForName:@"MoreAppVC"] bundle:nil];
    mvc.moreArray = [[NSMutableArray alloc] initWithArray:moreArray];
    [self presentViewController:mvc animated:YES completion:nil];
}

# pragma mark -
# pragma mark  MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

#pragma mark - Parser constants
// Reduce potential parsing errors by using string constants declared in a single place.
static NSString * const kName = @"Name";
static NSString * const kUrl = @"Url";
static NSString * const kImgUrl = @"Img";
static NSString * const kisPaid = @"isPaid";

# pragma mark -
# pragma mark  NSXMLParser delegate methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    NSLog(@"elementName=>%@ attributeDict=>%@",elementName,attributeDict);
    
    /* Use the flag didAbortParsing to distinguish between this deliberate stop and other parser errors. */
    
//    [parser abortParsing];
    if ([elementName isEqualToString:@"App"])
    {
        if(!tempDic)
            tempDic = [[NSMutableDictionary alloc] init];
        
        [tempDic setObject:[attributeDict valueForKey:kName] forKey:kName];
        [tempDic setObject:[attributeDict valueForKey:kUrl] forKey:kUrl];
        [tempDic setObject:[attributeDict valueForKey:kImgUrl] forKey:kImgUrl];
        [tempDic setObject:[attributeDict valueForKey:kisPaid] forKey:kisPaid];
        
        _accumulatingParsedCharacterData = YES;
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"App"]) {
        [moreArray addObject:tempDic];
    }
    
    tempDic = nil;
    // Stop accumulating parsed character data. We won't start again until specific elements begin.
    _accumulatingParsedCharacterData = NO;
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
}

/*
 An error occurred while parsing the data: post the error as an NSNotification to our app delegate.
 */
- (void)handleParseError:(NSError *)parseError {
    
}

/*
 An error occurred while parsing the data, pass the error to the main thread for handling.
 */
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    if ([parseError code] != NSXMLParserDelegateAbortedParseError && !_didAbortParsing)
    {
        [self performSelectorOnMainThread:@selector(handleParseError:) withObject:parseError waitUntilDone:NO];
    }
}

@end