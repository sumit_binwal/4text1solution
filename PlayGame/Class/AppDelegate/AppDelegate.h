//
//  AppDelegate.h
//  PlayGame
//
//  Created by Chandan Kumar on 04/03/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,MBProgressHUDDelegate>
{
    NSNumberFormatter * _priceFormatter;
    
    MBProgressHUD          *HUD;
    BOOL enableHUD ;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) HomeViewController *viewController;
@property (strong, nonatomic)  UINavigationController *navController;
@property (strong, nonatomic) NSNumberFormatter * _priceFormatter;

+(AppDelegate*) getSharedInstance;

-(void)winWatchVideoReward:(int)amount;

-(void) showHUDWithMessage:(NSString*) message;
-(void) hideHUD;

@end
