//
//  GetPremiumVC.m
//  FourPicsOneWord
//
//  Created by  on 10/16/13.
//  Copyright (c) 2013 Vojtec. All rights reserved.
//

#import "GetPremiumVC.h"
#import <StoreKit/StoreKit.h>
#import "RageIAPHelper.h"

@interface GetPremiumVC () <SKProductsRequestDelegate>

@end

@implementation GetPremiumVC

@synthesize backgroundImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [(UIImageView*)[self.view viewWithTag:100] setImage:[UIImage imageNamed:isIpad?@"upgrade_popup_iPad":isiPhone5?@"upgrade_popup_5":@"upgrade_popup"]];
    
    _topConstraint.constant = isiPhone5?240:195;
    _crossConstraint.constant = isiPhone5?40:18;
    
    //No ads
    [(UILabel*)[self.view viewWithTag:10000] setFont:[UIFont fontWithName:@"Romy" size:IS_IPAD?41:22]];
    [(UILabel*)[self.view viewWithTag:10000] setText:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"NoAds", nil)]];
    //Get Coins
    [(UILabel*)[self.view viewWithTag:20000] setFont:[UIFont fontWithName:@"Romy" size:IS_IPAD?41:22]];
    [(UILabel*)[self.view viewWithTag:20000] setText:[NSString stringWithFormat:@"   %@", NSLocalizedString(@"500Coins", nil)]];
    
    [[(UIButton*)[self.view viewWithTag:30000] titleLabel] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?25:15]];
    [self setUpgradeButton];
    
    [[(UIButton*)[self.view viewWithTag:40000] titleLabel] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?25:15]];
    [[(UIButton*)[self.view viewWithTag:50000] titleLabel] setFont:[UIFont fontWithName:@"RobotoSlab-Bold" size:IS_IPAD?25:15]];
   
    if (productArr.count==0) {
        [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
                productArr = products;
                [self setUpgradeButton];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark-
#pragma mark Other methods

#pragma mark InAppPurchase
- (void)productPurchased:(NSNotification *)notification {
    NSString * productIdentifier = notification.object;
    [productArr enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            
            *stop = YES;
        }
    }];
}

-(void)setUpgradeButton
{
    if (productArr.count) {
        for (SKProduct *product in productArr)
        {
            if ([product.productIdentifier isEqualToString:removeAddProductId])
            {
                [[AppDelegate getSharedInstance]._priceFormatter setLocale:product.priceLocale];
                [(UIButton*)[self.view viewWithTag:30000] setTitle:[NSString stringWithFormat:NSLocalizedString(@"Upgrade", nil), [[AppDelegate getSharedInstance]._priceFormatter stringFromNumber:product.price]] forState:UIControlStateNormal];
            }
        }
    }
    else
        [(UIButton*)[self.view viewWithTag:30000] setTitle:[NSString stringWithFormat:NSLocalizedString(@"Upgrade", nil), @"0"] forState:UIControlStateNormal];
}

#pragma mark-
#pragma mark IBActions
-(IBAction)ContinueButtonPressed
{
   [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)getPremiumButtonPressed
{
    if ([[RageIAPHelper sharedInstance] productPurchased:removeAddProductId]) {
        [[RageIAPHelper sharedInstance] restoreCompletedTransactions];
    }
    else
    {
        NSArray * productIdentifiers = @[ removeAddProductId,];
        
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
        productsRequest.delegate = self;
        [productsRequest start];
    }
}

-(IBAction)restoreButtonPressed
{
    if ([[RageIAPHelper sharedInstance] productPurchased:removeAddProductId]) {
        [[RageIAPHelper sharedInstance] restoreCompletedTransactions];
    }
    else
    {
        NSArray * productIdentifiers = @[ removeAddProductId,];
        
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
        productsRequest.delegate = self;
        [productsRequest start];
    }
}

- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
   if (response.products.count < 1 || ![((SKProduct*)response.products[0]).productIdentifier isEqualToString:removeAddProductId]) {
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There was an error while trying do upgrade to premium version" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
      [alert show];
      [self dismissViewControllerAnimated:YES completion:nil];
      return;
   }

   SKPayment *payment = [SKPayment paymentWithProduct:response.products[0]];
   [[SKPaymentQueue defaultQueue] addPayment:payment];
   [self dismissViewControllerAnimated:YES completion:nil];
}

@end
